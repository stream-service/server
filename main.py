import os
from urllib.request import Request

from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware

from pydantic_aioredis import RedisConfig

import config_manager
from config_manager.exceptions import (
    StreamNotFoundError,
    RTSPURLValidationError
)
from config_manager.models import StreamCreateModel, StreamModel


REDIS_PARAMS = {
    'host': os.getenv('REDIS_HOST', 'localhost'),
    'port': int(os.getenv('REDIS_PORT', '6379')),
    'password': os.getenv('REDIS_PASSWORD', None),
    'database': int(os.getenv('REDIS_DATABASE', '0'))
}

NIMBLE_MANAGE_URL = os.getenv("NIMBLE_MANAGE_URL")
NIMBLE_MANAGE_TOKEN = os.getenv("NIMBLE_MANAGE_TOKEN")
NIMBLE_CONFIG_PATH = os.getenv('NIMBLE_CONFIG_PATH')

redis_config=RedisConfig(**REDIS_PARAMS)

manager = config_manager.Manager(
    redis_config,
    NIMBLE_CONFIG_PATH,
    NIMBLE_MANAGE_URL,
    NIMBLE_MANAGE_TOKEN
)

app = FastAPI(
    title='Nimble config manager',
    version=config_manager.VERSION
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)

@app.exception_handler(StreamNotFoundError)
async def stream_not_found_exception_handler(request: Request, err: StreamNotFoundError):
    return JSONResponse(
        status_code=404,
        content={
            'error': str(err)
        }
    )

@app.exception_handler(RTSPURLValidationError)
async def rtsp_validation_exception_handler(request: Request, err: RTSPURLValidationError):
    return JSONResponse(
        status_code=400,
        content={
            'error': str(err)
        }
    )

@app.exception_handler(RuntimeError)
async def runtime_exception_handler(request: Request, err: RuntimeError):
    print(f'Unexpected error: {err}')
    return JSONResponse(
        status_code=500,
        content={
            'error': str(err)
        }
    )

@app.get("/streams", response_model=list[StreamModel])
async def get_streams():
    return await manager.get_streams()

@app.post("/stream")
async def create_stream(stream: StreamCreateModel):
    return await manager.create_stream(stream)

@app.put("/stream/{stream_id}")
async def update_stream(stream_id: str, stream: StreamModel):
    return await manager.update_stream(stream_id, stream)

@app.get("/stream/{stream_id}")
async def get_stream(stream_id: str):
    response = await manager.get_stream(stream_id)

    if response is None:
        raise StreamNotFoundError(stream_id)

    return response[0]

@app.delete("/stream/{stream_id}")
async def delete_stream(stream_id: str):
    result = await manager.delete_stream(stream_id)

    return {'status': 'ok' if result else 'error'}

# @app.post("/reload")
# async def reload_server():
#     result = await server.update_config()
#     return {'status': 'ok' if result else 'error'}

@app.get('/version')
async def get_version():
    return {'version': manager.get_version()}
