class RTSPURLValidationError(RuntimeError):
    def __init__(self, url: str) -> None:
        self.message = 'Invalid RTSP URL'
        self.url = url

        super().__init__(self.message)

    def __str__(self):
        return f'{self.message}: {self.url}'

class StreamNotFoundError(RuntimeError):
    def __init__(self, stream_id: str) -> None:
        self.message = 'Stream not found'
        self.stream_id = stream_id

        super().__init__(self.message)

    def __str__(self):
        return f'{self.message}: {self.stream_id}'
