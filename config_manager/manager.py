import json
import math
import os
import time

from urllib.parse import urlparse
import aiofiles
from fastapi.encoders import jsonable_encoder
from pydantic_aioredis import RedisConfig, Store

from nimble_streamer_client import NimbleAPIClient
import config_manager
from config_manager.exceptions import StreamNotFoundError
from config_manager.models.stream import (
    StreamModel,
    StreamCreateModel
)

def validate_rtsp_url(url: str) -> None: # TODO: validate url
    print(urlparse(url))

class Manager():
    def __init__(self, redis_config: RedisConfig, config_path: str, manage_url: str, manage_token: str = "") -> None:

        self._config_path = config_path

        self._manage_url = manage_url

        self._manage_token = manage_token

        _store = Store(name='stream_server', redis_config=redis_config)

        _store.register_model(StreamModel)

        self.rtmp_settings = {
            "interfaces": {
                "ip": "0.0.0.0",
                "port": 1935
            },
            "login": "",
            "password": "",
            "duration": 6,
            "chunk_count": 4,
            "dash_template": "TIME",
            "protocols": [
                "RTMP",
                "RTSP",
                "SLDP"
            ],
            "apps": [
                {
                    "app": "static",
                    "login": "",
                    "password": "",
                    "duration": 1,
                    "chunk_count": 1,
                    "dash_template": "TIME",
                    "protocols": [
                        "RTMP",
                        "RTSP",
                        "SLDP"
                    ]
                }
            ]
        }

        self.rtsp_settings = {
            "interfaces": [
                {
                    "ip": "0.0.0.0",
                    "port": 554
                }
            ]
        }

        self.dvr_settings = {
            "settings": [
                {
                    "app": "static",
                    "stream": "",
                    "thread_name": "default",
                    "path": "/home/dvr/",
                    "read_only": False,
                    "drop_invalid_segments": False,
                    "keep_proto_timestamps": False,
                    "check_segment_sizes_on_load": False,
                    "priority": 0,
                    "max_duration": 44640,
                    "max_size": 0,
                    "segment_duration": 60
                }
            ]
        }

    async def get_streams(self):
        return await StreamModel.select() or []

    async def create_stream(self, new_stream: StreamCreateModel):
        stream = StreamModel(**new_stream.dict())

        for url in stream.urls:
            validate_rtsp_url(url)

        await StreamModel.insert(stream)

        await self.update_config()

        return stream

    async def update_stream(self, stream_id: str, stream: StreamModel):
        if await self.get_stream(stream_id) is None:
            raise StreamNotFoundError(stream_id)

        await stream.save()

        await self.update_config()

        return stream

    async def get_stream(self, stream_id: str):
        return await StreamModel.select(ids=[stream_id])

    async def delete_stream(self, stream_id: str):
        if await self.get_stream(stream_id) is None:
            raise StreamNotFoundError(stream_id)

        await StreamModel.delete(ids=[stream_id])

        await self.update_config()

        return True

    async def update_config(self) -> bool:
        settings_hash = str(math.trunc(time.time())).encode('UTF-8')

        streams = await self.get_streams()

        json_obj = {
            "SyncResponse": {
                "status": "success",
                "RtmpSettings": self.rtmp_settings | {"hash": settings_hash},
                "RtspSettings": self.rtsp_settings | {"hash": settings_hash},
                "LivePullSettings": {
                    "hash": settings_hash,
                    "streams": streams
                },
                "DvrSettings": self.dvr_settings
            }
        }

        async with aiofiles.open(self._config_path, "w") as fp:
            await fp.write(json.dumps(jsonable_encoder(json_obj)))

        async with NimbleAPIClient(self._manage_url, self._manage_token) as api:
            response = await api.reload_config()

            print(f'reload_config API response: {response}')

        return True

    async def get_server_status(self):
        async with NimbleAPIClient(self.manage_url, self.manage_token) as api:
            return await api.server_status()

    def get_version(self):
        return config_manager.VERSION