import hashlib
import math
import time
from pydantic import Field
from pydantic_aioredis import Model

__all__ = [
    'StreamCreateModel',
    'StreamModel'
]

def generate_stream_id():
    hexdigest = hashlib.md5(str(time.time()).encode('UTF-8')).hexdigest()
    timestamp = math.trunc(time.time())

    return f"{hexdigest}_{timestamp}"

class StreamCreateModel(Model):
    name: str
    urls: list[str]
    app: str = 'static'

class StreamModel(StreamCreateModel):
    _primary_key_field: str = 'id'
    id: str = Field(default_factory=generate_stream_id)
